// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
// router setup
import router from './router/router'
// jQuery
global.jQuery = jQuery
global.$ = jQuery
var $ = global.jQuery
window.$ = $
window.Vue = Vue
// Plugins
//--Primary Plugin //
import vueResource from 'vue-resource' //Data Resource //
import DatatableFactory from 'vuejs-datatable' // Data Table //
//--Plus Plugin //
// import VueApexCharts from 'vue-apexcharts' // // Chart Apex //
import moment from 'moment' // Moment Js //
import axios from 'axios' // Axios Api Rest //
//GLOBAL FILE//
import globalFile from './router/globalFile'

// Gunakan yang sudah di import //
//--Core//
Vue.use(vueResource)
Vue.use(DatatableFactory)
Vue.use(globalFile)
Vue.config.productionTip = false
//--Plus//
//Vue.use(VueApexCharts)//
Vue.use(moment)
Vue.use(axios)
export const bus = new Vue();


new Vue({
    el: '#app',
    router,
    render: h => h(App),
    components: { App },
    template: '<App/>',
    
})
