import Vue from 'vue'
import Router from 'vue-router'
/*MODULE*/
import Hello from 'page@/Hello'
import Test from 'page@/test'
import Users from 'page@/users'

import Home from 'page@/home'
import scanQr from 'page@/scanQr'
import masukanKode from 'page@/masukKode'
// Belajar //
import moment from 'component@/plugin/moment'
import apilist from 'page@/apiList'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: __dirname,
routes: [
    // Index Page //
    {path: '/', redirect: '/index'},{path: '/index', redirect: '/home'},
    {path: '/hello', name: 'Hello', component: Hello},
    {path: '/test', name: 'Test', component: Test},
    {path: '/users', name: 'Users', component: Users},
    // Home //
    {path: '/home', name: 'home', component: Home},
    {path: '/scanQr', name: 'scanQr', component: scanQr},
    {path: '/masukanKode', name: 'masukanKode', component: masukanKode},
    {path: '/berhasil',redirect: '/home'},
    // Test Plugin //
    {path: '/moment', name: 'moment', component: moment},
    // Api Tester //
    {path: '/apilist', name: 'apilist', component: apilist},
]
});